<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;
use App\Models\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['Products'] = Product::with('category')->search($request->search)->paginate(10);

        return view('admin.product.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['Categories'] = Category::SortByName()->get();

        return view('admin.product.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'product_name' => 'required|max:100',
            'category' => 'required|exists:categories,category_id',
            'product_price' => 'required|min:0'
        ]);

        $Product = new Product;
        $Product->product_name = $request->product_name;
        $Product->product_description = $request->product_description;
        $Product->product_price = $request->product_price;
        $Product->category_id = $request->category;
        $Product->save();

        return redirect()->route('product.index')->with(['message' => 'Product '.$Product->product_name.' created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['Product'] = Product::findOrFail($id);
        $data['Categories'] = Category::SortByName()->get();

        return view('admin.product.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'product_name' => 'required|max:100',
            'category' => 'required|exists:categories,category_id',
            'product_price' => 'required|min:0'
        ]);

        $Product = Product::findOrFail($id);
        $Product->product_name = $request->product_name;
        $Product->product_description = $request->product_description;
        $Product->product_price = $request->product_price;
        $Product->category_id = $request->category;
        $Product->save();

        return redirect()->route('product.index')->with(['message' => 'Product '.$Product->product_name.' updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Product = Product::findOrFail($id);
        $Product->delete();

        return redirect()->route('product.index')->with(['message' => 'Product '.$Product->product_name.' deleted']);
    }
}
