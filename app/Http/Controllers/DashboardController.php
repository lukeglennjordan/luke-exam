<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;
use App\Models\Category;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	$data['ProductCount'] = Product::count();
    	$data['CategoryCount'] = Category::count();

        return view('admin.dashboard', $data);
    }
}
