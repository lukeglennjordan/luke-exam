<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['Categories'] = Category::search($request->search)->paginate(10);

        return view('admin.category.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'category_name' => ['required', Rule::unique('categories')->whereNull('deleted_at')]
        ]);

        $Category = new Category;
        $Category->category_name = $request->category_name;
        $Category->category_description = $request->category_description;
        $Category->save();

        return response()->json(['message' => 'Category '.$Category->category_name.' created', 'status' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['Category'] = Category::findOrFail($id);

        return view('admin.category.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Category = Category::findOrFail($id);

        $validated = $request->validate([
            'category_name' => ['required', Rule::unique('categories')->ignore($Category->category_id, 'category_id')->whereNull('deleted_at')]
        ]);

        $Category->category_name = $request->category_name;
        $Category->category_description = $request->category_description;
        $Category->save();

        return response()->json(['message' => 'Category '.$Category->category_name.' updated', 'status' => 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Category = Category::findOrFail($id);
        $Category->delete();

        return response()->json(['message' => 'Category '.$Category->category_name.' deleted', 'status' => 1]);
    }
}
