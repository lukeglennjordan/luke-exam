<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
class Category extends Model
{
    use SoftDeletes;
    
    protected $table = 'categories';
    public $timestamps = true;
    public $primaryKey = 'category_id';

    public function scopeSortByName($query)
    {
    	return $query->orderBy('category_name', 'ASC');
    }

    public function scopeSearch($query, $search)
    {
    	if($search)
    	{
    		return $query->whereRaw("MATCH(category_name,category_description) AGAINST(?)", array($search));
    	}
    }

}
