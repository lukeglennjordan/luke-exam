<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\Models\Category;
class Product extends Model
{
    use SoftDeletes;
    
    protected $table = 'products';
    public $timestamps = true;
    public $primaryKey = 'product_id';


    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'category_id')->withTrashed();
    }

    public function scopeSearch($query, $search)
    {
    	if($search)
    	{
    		return $query->whereRaw("MATCH(product_name,product_description) AGAINST(?)", array($search));
    	}
    }
}
