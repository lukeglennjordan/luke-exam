<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTblCategoryAddFullTextSearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::statement('ALTER TABLE `categories` ADD FULLTEXT INDEX search_full_index (category_name, category_description)');
        DB::statement('ALTER TABLE `products` ADD FULLTEXT INDEX search_full_index (product_name, product_description)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function($table) {
            $table->dropIndex('search_full_index');
        });

        Schema::table('products', function($table) {
            $table->dropIndex('search_full_index');
        });
    }
}
