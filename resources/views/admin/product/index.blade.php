@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if(session()->has('message'))
                 <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>{{session('message')}}</div>
            @endif

            <div class="card">
                <div class="card-header">Product List <a class="btn btn-primary btn-sm float-right" href="{{route('product.create')}}">Create</a></div>

                <div class="card-body table-responsive">
                    <div>
                        <form method="GET" action="{{route('product.index')}}">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search" name="search" value="{{ request()->input('search', old('search')) }}">
                                <div class="input-group-append">
                                  <button class="btn btn-secondary" type="submit">
                                    Search
                                  </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br />
                    <table class="table table-bordered ">
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th></th>
                        </tr>

                        <tbody>
                            @if(count($Products) >= 1)
                                @foreach($Products as $key => $Product)
                                    <tr>
                                        <td>{{$Product->product_name}}</td>
                                        <td>{{number_format($Product->product_price, 2)}}</td>
                                        <td>{{$Product->product_description}}</td>
                                        <td>{{$Product->category->category_name}}</td>
                                        <td>
                                            <center>
                                                <form class="product-form-{{$Product->product_id}}" method="post" action="{{route('product.destroy', $Product->product_id)}}">
                                                    {!! csrf_field() !!} {{ method_field('DELETE') }}
                                                </form>
                                                <a class='btn btn-primary btn-sm' href="{{route('product.edit', $Product->product_id)}}">Edit</a>
                                                <button class="btn btn-danger btn-sm btn-delete-product" name="{{$Product->product_name}}" id="{{$Product->product_id}}" >Delete</button>
                                            </center>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan="20"><center>--No Data Found--</center></td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer"><center>{{ $Products->appends(request()->query())->links() }}</center></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<link rel="stylesheet" href="{{ asset('js/jquery/jquery.confirm/jquery-confirm.min.css')}}">
<script src="{{ asset('js/jquery/jquery.confirm/jquery-confirm.min.js')}}"></script>
<script src="/js/admin/product.js?v=1.09"></script>
@endsection