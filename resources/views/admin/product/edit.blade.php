@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4 mx-auto">
            <form method="POST" action="{{route('product.update', $Product->product_id)}}">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <div class="card">
                    <div class="card-header">Update Product</div>

                    <div class="card-body">
                        <div>
                            <label>Name</label>
                            <input type="text" class="form-control" name="product_name" required="required" value="{{ request()->input('product_name', old('product_name', $Product->product_name)) }}">
                        </div>
                        <div>
                            <label>Description</label>
                            <textarea class="form-control" name="product_description" required="required">{{ request()->input('product_description', old('product_description', $Product->product_description)) }}</textarea>
                        </div>
                        <div>
                            <label>Price</label>
                            <input type="number" min="0" step="any" class="form-control" name="product_price" required="required" value="{{ request()->input('product_price', old('product_price', $Product->product_price)) }}">
                        </div>
                        <div>
                            <label>Category</label>
                            <select class="form-control" name="category">
                                @foreach($Categories as $key => $value)
                                    <option value="{{$value->category_id}}" {{ request()->input('product_price', old('product_price', $Product->category_id)) == $value->category_id ? 'selected' : '' }}>{{$value->category_name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="card-footer">
                        @if($errors->any())
                            <div class="alert alert-danger col-md-12">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <button class='btn btn-primary float-right' type="submit">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
