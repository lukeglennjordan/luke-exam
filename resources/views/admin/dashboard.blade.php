@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>Total Products: <a href="{{route('product.index')}}">{{$ProductCount}}</a></div>
                    <div>Total Categories: <a href="{{route('category.index')}}">{{$CategoryCount}}</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
