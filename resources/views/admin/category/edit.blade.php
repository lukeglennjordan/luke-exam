<form method="POST" class="form-category-create" action="{{route('category.update', $Category->category_id)}}">
    {!! csrf_field() !!}
    {{ method_field('PUT') }}
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>
            <label>Name</label>
            <input type="text" class="form-control" name="category_name" required="required" value="{{ request()->input('category_name', old('category_name', $Category->category_name)) }}">
        </div>
        <div>
            <label>Description</label>
            <textarea class="form-control" name="category_description" >{{ request()->input('category_description', old('category_description', $Category->category_description)) }}</textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-create-category"  >Save</button>
      </div>
    </div>
</form>