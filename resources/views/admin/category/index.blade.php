@extends('layouts.app')

@section('content')
<div class="reload-data">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if(session()->has('message'))
                 <div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>{{session('message')}}</div>
            @endif

            <div class="card ">
                <div class="card-header">Category List <button class="btn btn-primary btn-sm float-right btn-category-popup" data-toggle="modal" data-target="#createCategoryModal" href="{{route('category.create')}}">Create</button></div>

                <div class="card-body table-responsive">
                    <div>
                        <form method="GET" class='search-no-refresh-category' action="{{route('category.index')}}">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search" name="search" value="{{ request()->input('search', old('search')) }}">
                                <div class="input-group-append">
                                  <button class="btn btn-secondary" type="submit">
                                    Search
                                  </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br />
                    <table class="table table-bordered ">
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th></th>
                        </tr>

                        <tbody class="reload-body">
                            @if(count($Categories) >= 1)
                                @foreach($Categories as $key => $category)
                                    <tr>
                                        <td>{{$category->category_name}}</td>
                                        <td>{{$category->category_description}}</td>
                                        <td>
                                            <center>
                                                <button type="button" class='btn btn-primary btn-sm btn-category-popup' data-toggle="modal" data-target="#createCategoryModal" href="{{route('category.edit', $category->category_id)}}">Edit</button>
                                                <a class="btn btn-danger btn-sm btn-delete-category" action="{{route('category.destroy', $category->category_id)}}" name="{{$category->category_name}}" id="{{$category->category_id}}" >Delete</a>
                                            </center>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan="20"><center>--No Data Found--</center></td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer"><center>{{ $Categories->appends(request()->query())->links() }}</center></div>
            </div>
        </div>
    </div>
</div>



    <!-- Modal -->
    <div class="modal fade" id="createCategoryModal" tabindex="-1" role="dialog" aria-labelledby="createCategoryModal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-category" role="document">
        <form method="POST" class="form-category-create" action="{{route('category.store')}}">
            {!! csrf_field() !!}
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Category Create</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div>
                    <label>Name</label>
                    <input type="text" class="form-control category_name_input_create" name="category_name" required="required" value="{{ request()->input('category_name', old('category_name')) }}">
                </div>
                <div>
                    <label>Description</label>
                    <textarea class="form-control category_description_input_create" name="category_description">{{ request()->input('category_description', old('category_description')) }}</textarea>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-create-category"  >Save</button>
              </div>
            </div>
        </form>   
      </div>
    </div>

    <form class="category-form-delete" method="post" action="">
        {!! csrf_field() !!} {{ method_field('DELETE') }}
    </form>

</div>

@endsection

@section('scripts')
<link rel="stylesheet" href="{{ asset('js/jquery/jquery.confirm/jquery-confirm.min.css')}}">
<link rel="stylesheet" href="{{ asset('js/jquery/toast/jquery.toast.min.css')}}">
<script src="{{ asset('js/jquery/jquery.confirm/jquery-confirm.min.js')}}"></script>

<script src="{{ asset('js/jquery/popper.js/js/popper.min.js')}}"></script>
<script src="{{ asset('js/jquery/bootstrap/js/bootstrap.min.js')}}"></script>

<script src="{{ asset('js/jquery/toast/jquery.toast.min.js')}}"></script>
<script src="/js/admin/category.js?v=1.09"></script>
@endsection