
$( document ).ready(function($) {

    confirmDelete();

    function confirmDelete()
    {
      $('.btn-delete-product').on('click', function(){
        let name = $(this).attr('name');
        let id = $(this).attr('id');

        $.confirm({
          title: '',
          content: 'Are you sure you want to delete product ' + name,
          buttons: {
              confirm: {
                    text: 'Confirm',
                    btnClass: 'btn btn-danger',
                    keys: ['enter', 'shift'],
                    action: function(){
                      $('.product-form-' + id).submit();   
                    }
                },
              cancel: function () {
                  
              }
          }
        });
      });
    }
    
});
