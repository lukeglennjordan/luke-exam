
$( document ).ready(function($) {

    let globalLink = '/category';
    reloadFunctions();

    function reloadFunctions()
    {
      $('.btn-delete-category').on('click', function(){
        let name = $(this).attr('name');
        let id = $(this).attr('id');
        let action = $(this).attr('action');

        $.confirm({
          title: '',
          content: 'Are you sure you want to delete category: ' + name,
          buttons: {
              confirm: {
                    text: 'Confirm',
                    btnClass: 'btn btn-danger',
                    keys: ['enter', 'shift'],
                    action: function(){
                      $('.category-form-delete').attr('action', action);
                      $('.category-form-delete').submit();
                    }
                },
              cancel: function () {
                  
              }
          }
        });
      });

      $('.btn-category-popup').on('click', function(event){
        let url = $(this).attr('href');
        $.get( url, function( data ) {
          $( ".modal-dialog-category" ).html( data );
          submitFormFunction();
        });
      });
      

      $('.page-link').on('click', function(event){

        event.preventDefault();
        var href = $(this).attr('href');

        reloadMainData(href);

      });

      $('.category-form-delete').on('submit', function(event){
        event.preventDefault();

        let url = $(this).attr('action');
        let type = $(this).attr('method');
        var formdata = new FormData($(this)[0]);

        $.ajax({
            url     :   url,
            type    :   type,
            data    :   formdata,
            processData: false,
            contentType: false,
            success :   function(data)
            {
              $.toast({
                  heading: 'Success',
                  text: data.message,
                  showHideTransition: 'fade',
                  icon: 'success',
                  position: 'top-right'
              });
              reloadMainData(globalLink);

            },
            error   :   function(Err)
            {
              
            }});
        });
    }
    

    function submitFormFunction()
    {
      $('.form-category-create').on('submit', function(event){
          event.preventDefault();

          let url = $(this).attr('action');
          let type = $(this).attr('method');
          var formdata = new FormData($(this)[0]);
          console.log(formdata);
          

          $.ajax({
            url     :   url,
            type    :   type,
            data    :   formdata,
            processData: false,
            contentType: false,
            success :   function(data)
            {
              $('#createCategoryModal').modal('hide');

              // MODAL VERSION BUG
              $('body').removeClass('modal-open');
              $('.modal-backdrop').remove();
              // END
              $.toast({
                  heading: 'Success',
                  text: data.message,
                  showHideTransition: 'fade',
                  icon: 'success',
                  position: 'top-right'
              });

              reloadMainData(globalLink);

            },
            error   :   function(Err)
            {
              let error = Err.responseJSON.errors;

              $.each(error, function(index, value) {
                $.toast({
                    heading: 'Error',
                    text: value,
                    showHideTransition: 'fade',
                    icon: 'error',
                    position: 'top-right'
                });
              });
            }});
        });
    }
    

    function reloadMainData(link){
      globalLink = link;

      $('.reload-body').html('<tr><td colspan="20"><center><div class="spinner-border" role="status"><span class="sr-only">Loading...</span> </div></center></td></tr>');
      $.get( link, function( data ) {
        $( ".reload-data" ).html( $(data).find('.reload-data') );
        reloadFunctions();
      });
    }
});
